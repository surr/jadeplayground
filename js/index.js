var $container = $('#content');

$.get('/templates/hello.jade')
.then(function(template){

    var render = jade.compile(template),
        html = render({name:'Superman'});

    $container.html(html);
})
