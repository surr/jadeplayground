var express = require('express'),
    jade = require('jade'),
    fs = require('fs'),
    path = require('path');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use('/templates', express.static( path.join(__dirname, 'templates') ));
app.use('/js', express.static( path.join(__dirname, 'js') ));
app.use('/css', express.static( path.join(__dirname, 'css') ));

app.get('/', function (req, res) { res.render('index') });

app.listen(4000, 'localhost');
